import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { Seccion1Component } from './seccion1/seccion1.component';
import { Seccion2Component } from './seccion2/seccion2.component';
import { Seccion3Component } from './seccion3/seccion3.component';
import { ThemeToolComponent } from './theme-tool/theme-tool.component';

const appRoutes: Routes = [
  {
    path: 'seccion1',
    component: Seccion1Component
  },
  {
    path: 'seccion2',
    component: Seccion2Component
  },
  {
    path: 'seccion3',
    component: Seccion3Component
  },
  {
    path: '',
    redirectTo: '/seccion1',
    pathMatch: 'full'
  }
  /*{ path: '**', component: ErrorComponent }*/
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    Seccion1Component,
    Seccion2Component,
    Seccion3Component,
    ThemeToolComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
